jQuery("document").ready(function($){

	"use strict";
	var isMobile = (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/Android/i)) || (navigator.userAgent.match(/Blackberry/i)) || (navigator.userAgent.match(/Windows Phone/i) ) ? true : false;


	// Counter
	$('.dt-sc-counter').each(function(){
		var $posttext = $(this).find('.dt-sc-counter-number').attr('data-append');
		var $append = ''; 

		if (typeof $posttext === "undefined") {
			$append = $.animateNumber.numberStepFactories.append('');
		} else {
			$append = $.animateNumber.numberStepFactories.append($posttext);
		}

		$(this).one('inview', function (event, visible) {
			if(visible === true) {
				var val = $(this).find('.dt-sc-counter-number').attr('data-value');
				$(this).find('.dt-sc-counter-number').animateNumber({ number: val, numberStep: $append }, 2000);
			}
		});
	});

	// Counter Type 4 Hover
	$('.dt-sc-counter.type4').hover(
		function(){
			var $bg = $(this).attr('data-hcolor');
			$(this).find('.dt-sc-couter-icon-holder').css({'background':$bg});
		},
		function(){
			$(this).find('.dt-sc-couter-icon-holder').removeAttr('style');
		}
	);

	// Icon Box 
	if( $('.dt-sc-icon-box-type9').length ) {
		setTimeout(function(){
			$('.dt-sc-icon-box-type9').each(function(){
				$(this).find('.icon-wrapper').css('height', $(this).find('.icon-content').outerHeight(true) );
			});
		},1000);
	}

	// Image caption
	$(".dt-sc-image-caption").each(function(){

		var $this = $(this),
			$open = $(this).find('.icon-wrapper > span.icon-info'),
			$close = $(this).find('.icon-wrapper > span.fa-close');

		$open.click(function(){
			$this.addClass('dt-sc-image-caption-hover');
		});

		$close.click(function(){
			$this.removeClass('dt-sc-image-caption-hover');
		});
	});

	// Poses Sorting
	$("div.dt-sc-poses-sorting > a").click(function(e){
		$("div.dt-sc-poses-sorting > a").removeClass("active-sort");
		$(this).addClass("active-sort");

		var $tax = $(this).attr("data-id"),
			$container = $(this).parents(".dt-sc-poses-sorting").next(".dt-sc-poses-container"),
			$column = $container.attr("data-column");

		$.ajax({
			url  : kriya_urls.ajax_url,
			data : {
				'action' : 'dt_sc_filter_poses',
				'data'   : {
					'tax' : $tax,
					'column' : $column
				}
			},
			beforeSend: function() {
				$container.html('<div class="dt-sc-loading"><span></span></div>');
			},
			success: function( response ) {
				$container.html(response).fadeIn();
			}
		});		
		e.preventDefault();
	});

	// Courses Sorting
	$("div.dt-sc-courses-sorting > a").click(function(e){
		$("div.dt-sc-courses-sorting > a").removeClass("active-sort");
		$(this).addClass("active-sort");

		var $id = $(this).attr("data-id"),
			$container = $(this).parents(".dt-sc-courses-sorting").next(".dt-sc-courses-container"),
			$term = $container.attr('data-term'),
			$column = $container.attr("data-column");

		$.ajax({
			url  : kriya_urls.ajax_url,
			data : {
				'action' : 'dt_sc_filter_courses',
				'data'   : {
					'id' : $id,
					'term' : $term,
					'column' : $column
				}
			},
			beforeSend: function() {
				$container.html('<div class="dt-sc-loading"><span></span></div>');
			},
			success: function( response ) {
				$container.html(response).fadeIn();
			}
		});		
		e.preventDefault();
	});

	// Video Manager
	if( $('div.dt-sc-videos-container').length ) {

		$(".video-overlay-inner a").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false,social_tools: false,deeplinking:false});

		$(".dt-sc-video-item").each(function(){
			$(this).click(function(){
				var $container = $(this).parents('.dt-sc-videos-container');
				$container.find("a").attr('href', $(this).attr('data-link') );
				$container.find(".dt-sc-video-wrapper").find("img").attr('src', $(this).find('img').attr('data-full') );
				$container.find(".dt-sc-video-wrapper").find('h2').html( $(this).find("h2").html() );
				$(this).parents('.dt-sc-videos').find('.active').removeClass('active');
				$(this).addClass('active');
			});
		});
	}

	$(window).load(function(){

		/* partners carousel */
		if($(".dt-sc-partners-carousel").length) {
			$(".dt-sc-partners-carousel").each(function(){

				var $prev = $(this).parents(".dt-sc-partners-carousel-wrapper").find(".partners-prev");
				var $next = $(this).parents(".dt-sc-partners-carousel-wrapper").find(".partners-next");
				var $scroll = $(this).parents(".dt-sc-partners-carousel-wrapper").attr('data-scroll');
				var $visible = $(this).parents(".dt-sc-partners-carousel-wrapper").attr('data-visible');
				
				$scroll = isMobile ? '1' : $scroll;
				$visible = isMobile ? '1' : $visible;
				
				$(this).carouFredSel({
					responsive: true,
					auto: false,
					width: '100%',
					height: 'variable',
					prev: $prev,
					next: $next,
					scroll: parseInt($scroll), // The number of items to scroll at once
					items: {
						visible:{
							min: parseInt($visible) // The number of items to show at once
						}
					}
				});
			});
		}

		/* images carousel */
		if($(".dt-sc-images-carousel").length) {
			$(".dt-sc-images-carousel").each(function(){

				var $prev = $(this).parents(".dt-sc-images-wrapper").find(".images-prev");
				var $next = $(this).parents(".dt-sc-images-wrapper").find(".images-next");

				$(this).carouFredSel({
					responsive: true,
					auto: false,
					width: '100%',
					height: 'variable',
					prev: $prev,
					next: $next,
					scroll: 1,
					items: {
						width:570,
						height: 'variable',
						visible: { min: 1, max: 1 }
					}
				});
			});
		}

		/* testimonial carousel */
		if( $('.dt-sc-testimonial-wrapper').length ) {

			$(".dt-sc-testimonial-carousel").each(function(){

				var $prev = $(this).parents(".dt-sc-testimonial-wrapper").find(".testimonial-prev");
				var $next = $(this).parents(".dt-sc-testimonial-wrapper").find(".testimonial-next");
				var $this = $(this);

				if( $this.find('li').length > 1 ) {

					$this.carouFredSel({
						responsive: true,
						auto: false,
						width: '100%',
						prev: $prev,
						next: $next,
						height: 'variable',
						scroll: { easing: "linear", duration: 500 },
						items: { width: 1170, height: 'variable',  visible: { min: 1, max: 1 } }
					});
				}
			});
		}
	});
});